﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JongaAPI.Migrations
{
    public partial class v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    DeviceTokenId = table.Column<int>(nullable: false),
                    DeviceToken = table.Column<string>(nullable: true),
                    Platform = table.Column<string>(nullable: true),
                    PushToken = table.Column<string>(nullable: true),
                    IoTDeviceId = table.Column<string>(nullable: true),
                    DeviceVerified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceTokens",
                columns: table => new
                {
                    Id = table.Column<int>(maxLength: 450, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IssuedUtc = table.Column<DateTime>(nullable: false),
                    ExpiresUtc = table.Column<DateTime>(nullable: false),
                    Token = table.Column<string>(maxLength: 450, nullable: false),
                    DeviceId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceTokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmergencyContacts",
                columns: table => new
                {
                    Cellphone = table.Column<string>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UID = table.Column<string>(nullable: true),
                    Country_Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmergencyContacts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IoTDevices",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Device_Id = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Battery = table.Column<string>(nullable: true),
                    Armed = table.Column<bool>(nullable: false),
                    TimeStamp = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IoTDevices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserEmergencyJunctions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmergencyContactName = table.Column<string>(nullable: true),
                    EmergencyCellphone = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    UserUID = table.Column<string>(nullable: true),
                    EmergencyId = table.Column<int>(nullable: false),
                    EmergencyUID = table.Column<string>(nullable: true),
                    Country_Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserEmergencyJunctions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Cellphone = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Fullname = table.Column<string>(type: "varchar(MAX)", nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UId = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    AccessToken = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    Device_Id = table.Column<string>(nullable: true),
                    BirthDay = table.Column<string>(nullable: true),
                    UserProfileImage = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PaymentDetails = table.Column<string>(nullable: true),
                    NotificationSettings = table.Column<string>(nullable: true),
                    IsResettingPassword = table.Column<bool>(nullable: false),
                    ResettingWindowUntil = table.Column<DateTime>(nullable: false),
                    LastOTPCode = table.Column<string>(nullable: true),
                    OTPUsed = table.Column<bool>(nullable: false),
                    OTPVerified = table.Column<bool>(nullable: false),
                    OTPBlocked = table.Column<bool>(nullable: false),
                    OTPBlockedUntilUtc = table.Column<DateTime>(nullable: false),
                    NumberOfOTPSentInMinutes = table.Column<int>(nullable: false),
                    LastOTPSentTimeUtc = table.Column<DateTime>(nullable: false),
                    OTPSessionWindow = table.Column<DateTime>(nullable: false),
                    OTPVeriWinodow = table.Column<DateTime>(nullable: false),
                    NumberOfOTPVeriInMinutes = table.Column<int>(nullable: false),
                    EmergencyContactId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_EmergencyContacts_EmergencyContactId",
                        column: x => x.EmergencyContactId,
                        principalTable: "EmergencyContacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "DeviceTokens",
                columns: new[] { "Id", "DeviceId", "ExpiresUtc", "IssuedUtc", "Token" },
                values: new object[] { 1, "qwe", new DateTime(2019, 3, 26, 11, 40, 42, 384, DateTimeKind.Utc), new DateTime(2019, 3, 26, 11, 35, 42, 385, DateTimeKind.Utc), "dsfdsf" });

            migrationBuilder.InsertData(
                table: "Devices",
                columns: new[] { "Id", "DeviceToken", "DeviceTokenId", "DeviceVerified", "IoTDeviceId", "Platform", "PushToken", "UserId" },
                values: new object[] { "qwe", "dsfdsf", 1, false, null, "Android", "hdghe", 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AccessToken", "Address", "BirthDay", "Cellphone", "CountryCode", "Device_Id", "Email", "EmergencyContactId", "Fullname", "IsResettingPassword", "LastOTPCode", "LastOTPSentTimeUtc", "NotificationSettings", "NumberOfOTPSentInMinutes", "NumberOfOTPVeriInMinutes", "OTPBlocked", "OTPBlockedUntilUtc", "OTPSessionWindow", "OTPUsed", "OTPVeriWinodow", "OTPVerified", "Password", "PaymentDetails", "ResettingWindowUntil", "UId", "UserProfileImage" },
                values: new object[] { 1, "sdfwv", null, null, "0761672567", "27", "qwe", "root@gmail.com", null, "admin", false, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, 0, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "Sfwregs123", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Users_EmergencyContactId",
                table: "Users",
                column: "EmergencyContactId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DropTable(
                name: "DeviceTokens");

            migrationBuilder.DropTable(
                name: "IoTDevices");

            migrationBuilder.DropTable(
                name: "UserEmergencyJunctions");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "EmergencyContacts");
        }
    }
}

﻿using JongaAPI.Models;
using JongaAPI.Services;
using JongaAPI.Services.Helper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AspNetCoreWebService.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/v1/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        //private IAuthService _AuthService;
        private readonly Bugsnag.IClient _bugsnag;
        private readonly IAuthService _AuthService;

        public AuthController(Bugsnag.IClient client, IAuthService authService)
        {
            _AuthService = authService;
            _bugsnag = client;

        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult initial()
        {

            return Ok("This is the JONGA API!");

        }
        /*
        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult AuthenticateUser([FromBody] UserLogin userParam)
        {
            if (userParam == null)
                return BadRequest(_AuthService.AuthenticateUser(null, null, null, null));
            var user = _AuthService.AuthenticateUser(userParam.Cellphone, userParam.Password, userParam.Device_Id, userParam.country_code);
            if (!user.Meta.Success)
                return BadRequest(user);

            return Ok(user);
        }*/
    }
}

/*
//Tested,published
[HttpPost("otp/verify/registration"), AllowAnonymous]
public IActionResult VerifyOTP([FromBody]OTPCode code)
{
    if (code == null)
        return BadRequest(_AuthService.VerifyOTP(null, null, null, null));
    var result = _AuthService.VerifyOTP(code.otp_code, code.cellphone, code.country_code, code.device_id);
    if (!result.Meta.Success)
        return BadRequest(result);
    return Ok(result);
}

[HttpPost("otp/verify/password_reset"), AllowAnonymous]
public IActionResult VerifyPasswordOTP([FromBody] ResetPasswrodVerify reset)
{
    if (reset == null)
        return BadRequest("Invalid Request");
    var result = _AuthService.VerifyOTP(reset.otp_code, reset.Cellphone, reset.country_code);
    if (!result.Meta.Success)
        return BadRequest(result);
    return Ok(result);
}
//Tested,published
[HttpPost("otp/send/registration"), AllowAnonymous]
public IActionResult SendOTP([FromBody] CellphoneOTPRequest cellphone)
{
    if (cellphone == null)
        return BadRequest("Invalid Request");
    var result = _AuthService.SendSMSOTP(cellphone.country_code, cellphone.Cellphone, true);
    if (!result.Meta.Success)
        return BadRequest(result);
    return Ok(result);
}

//Tested,published
[HttpPost("otp/send/password_reset"), AllowAnonymous]
public IActionResult SendResetOTP([FromBody] ResetPasswrod reset)
{
    if (reset == null)
        return BadRequest(_AuthService.SendSMSOTP(null, null));

    var result = _AuthService.SendSMSOTP(reset.Country_Code, reset.Cellphone);
    if (!result.Meta.Success)
        return BadRequest(result);
    return Ok(result);
}



//Tested,published
[HttpPost("user_logout")]
public IActionResult LogoutUser([FromHeader] string Authorization)
{
    var result = _AuthService.LogoutUser(Authorization);
    if (!result.Meta.Success)
        return BadRequest(result);
    return Ok(result);
}

//Tested,published
[AllowAnonymous]
[HttpPost("login")]
public IActionResult AuthenticateUser([FromBody] UserLogin userParam)
{
    if (userParam == null)
        return BadRequest(_AuthService.AuthenticateUser(null, null, null, null));
    var user = _AuthService.AuthenticateUser(userParam.Cellphone, userParam.Password, userParam.Device_Id, userParam.country_code);
    if (!user.Meta.Success)
        return BadRequest(user);

    return Ok(user);
}

//Tested,published
[AllowAnonymous, HttpPost("device")]
public IActionResult AuthenticateDevice([FromHeader]string Authorization, [FromBody] DeviceAuthInput device)
{
    //Check to see if device is in database
    if (device == null)
        return BadRequest(_AuthService.AuthenticateDevice(null, null, null));
    var result = _AuthService.AuthenticateDevice(Authorization, device.Device_Id, device.Push_Token);
    if (!result.Meta.Success)
        return BadRequest(result);

    return Ok(result);
}
*//*

//Tested,published
[ValidateModel]
[AllowAnonymous]
[HttpPost("registration")]
public IActionResult AddUser([FromBody] NewUser userParam)
{
    if (userParam == null)
        return BadRequest("Invalid Request");
    var addedUser = _AuthService.AddUser(userParam);
    if (!addedUser.Meta.Success)
    {
        return BadRequest(addedUser);
    }
    return Ok(addedUser);
}


//Tested,published
[AllowAnonymous]
[HttpGet]
public IActionResult initial()
{

    return Ok("This is the JONGA API!");

}

[HttpPost("password_reset")]
public IActionResult ResetPassword([FromHeader] string Authorization, [FromBody] UserPassword pass)
{
    if (pass == null)
        return BadRequest(_AuthService.ResetMyPassword(Authorization, null));
    var res = _AuthService.ResetMyPassword(Authorization, pass.Password);
    if (!res.Meta.Success)
        return BadRequest(res);
    return Ok(res);
}*/


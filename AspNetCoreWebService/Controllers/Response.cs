﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JongaAPI.Controllers
{
    [Route("")]
    [ApiController]
    public class HoldingPage : ControllerBase
    {
        //Tested,published
        [AllowAnonymous]
        [HttpGet]
        public IActionResult initial()
        {
            return Ok("This is the JONGA API!");
        }

    }
}

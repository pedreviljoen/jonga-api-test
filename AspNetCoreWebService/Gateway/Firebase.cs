﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using JongaAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace JongaAPI.Gateway
{
    public static class Firebase
    {

        static Firebase()
        {
            FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile("jonga.json"),
            });
        }

        public static void SendPushNotiAsync(string PushToken)
        {
            string serverKey = "AAAAK6uk5AA:APA91bFJn4d_nTe3lVq9rhvXdjOCm3xx5Jmtbcnq4Y7pkEEJRBpsKzjSpNg1Sjpl-04fyeP2ViaIiL5lJvxA8YGbOpp_NMK_4O1C_6zyX93PMrnNM5sbLsn5hfWwSjPB-J4yMCtFp19k";


            var result = "-1";
            var webAddr = "https://fcm.googleapis.com/fcm/send";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"to\":  \"" + PushToken + "\"," +
                    "\"notification\":{" +

                    "\"title\": \"This is a Firebase Cloud Messaging Topic Message!\"," +
                    "\"body\": \"This is a Firebase Cloud Messaging Topic Message!\"}}";
                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            // return result;


        }

        public static async Task<bool> SendPushNotiAsync(Message msg)
        {
            var response = await FirebaseMessaging.DefaultInstance.SendAsync(msg);

            return true;
        }
    }
}

﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace JongaAPI.Gateway
{
    public class TwilioGateway
    {
        private const string AccountSID = "ACd5a2390bddee85340c621e9168ffa866"; //This is a  ACCOUNT!
        private const string AuthToken = "4c815f7def173911b3726165d0dbe641";
        private const string MyTwilioNumber = "+13234161736"; //THIS IS a  NUMBER

        public static void SendSMS(string SMS, string ToNumber)
        {
            TwilioClient.Init(AccountSID, AuthToken);
            var message = MessageResource.Create(
                body: SMS,
                from: new Twilio.Types.PhoneNumber(MyTwilioNumber),
                to: new Twilio.Types.PhoneNumber(ToNumber)
            );
        }
    }
}
﻿using JongaAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace JongaAPI
{
    public class UserDBContext : DbContext
    {
        public UserDBContext(DbContextOptions<UserDBContext> options) : base(options)
        {
        }

        /* protected override void OnModelCreating(ModelBuilder modelBuilder)
         {
             User TestUser = new User { Password = "Sfwregs123", Email = "root@gmail.com", Cellphone = "0761672567", Fullname = "admin", CountryCode = "27", Id = 1, AccessToken = "sdfwv" };

             DeviceToken TestDeviceToken = new DeviceToken { ExpiresUtc = DateTime.UtcNow.AddMinutes(5), IssuedUtc = DateTime.UtcNow, Token = "dsfdsf", DeviceId = "qwe", Id = 1 };
             PushToken TestPushToken = new PushToken { Token = "hdghe", DeviceId = "bhnaerq" };
             AccessToken TestAcess = new AccessToken { Token = "sdfsdfqw" };
             Device TestDevice = new Device { Id = "qwe", UserId = TestUser.Id, DeviceTokenId = TestDeviceToken.Id, PushToken = TestPushToken.Token, DeviceToken = TestDeviceToken.Token, Platform = "Android", };
             TestUser.Device_Id = TestDevice.Id;

             modelBuilder.Entity<User>().HasData(TestUser);
             modelBuilder.Entity<DeviceToken>().HasData(TestDeviceToken);

             modelBuilder.Entity<Device>().HasData(TestDevice);
         }*/

        public DbSet<User> Users { get; set; }
        public DbSet<DeviceToken> DeviceTokens { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<EmergencyContact> EmergencyContacts { get; set; }
        public DbSet<UserEmergencyJunction> UserEmergencyJunctions { get; set; }
        public DbSet<IoTDevice> IoTDevices { get; set; }
    }
}
﻿using JongaAPI.Services;
using JongaAPI.Services.Helper;
using JongaAPI.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Bugsnag.AspNet.Core;
using System;

namespace JongaAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //it should be at appsettings

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            Console.WriteLine(appSettings.ToString());
            //var connection = @"Server=(localdb)\MSSQLLocalDB;Database=UserAccountDB;Trusted_Connection=True;";
            //var connection = appSettingsSection.Get<AppSettings>().UserAccountDB;

            var connection = "Server=jonga-testdb.cee3wmkczals.us-east-2.rds.amazonaws.com;Database=jonga-testdb;User=liujiamo;Password=12345678;";

            var testconnection = "Data Source=jonga-testdb.cee3wmkczals.us-east-2.rds.amazonaws.com;Initial Catalog=jonga-testdb;User ID=liujiamo;Password=12345678;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            services.AddDbContext<UserDBContext>(
                options => options.UseSqlServer(testconnection)
                );

            services.AddBugsnag(configuration =>
            {
                configuration.ApiKey = "9e0a5d30764a3f9b0a2f079765597859";
            });

            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // configure DI for application services
            //services.AddScoped<IIoTService, IoTService>();
            services.AddScoped<IAuthService, AuthService>();
            //services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IDeviceService, DeviceService>();
            //services.AddScoped<IIoTDeviceService, IoTDeviceService>();





        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            // UpdateDatabase(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseHsts();
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            app.UseMvc();
        }

        // private static void UpdateDatabase(IApplicationBuilder app)
        // {
        //     using (var serviceScope = app.ApplicationServices
        //         .GetRequiredService<IServiceScopeFactory>()
        //         .CreateScope())
        //     {
        //         using (var context = serviceScope.ServiceProvider.GetService<UserDBContext>())
        //         {
        //             context.Database.Migrate();
        //         }
        //     }
        // }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JongaAPI.Models
{
    public class EmergencyContact
    {
        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Cellphone { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string UID { get; set; }
        public string Country_Code { get; set; }

        public List<User> Users { get; set; }
    }

    public class AddEmergencyContact
    {
        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Fullname { get; set; }

        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Cellphone { get; set; }

        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Country_Code { get; set; }
    }

    public class UpdateEmergencyContact
    {
        public string UId { get; set; }
        public string Name { get; set; }
        public string Cellphone { get; set; }
        public string Country_Code { get; set; }
    }

    public class DeleteEmergencyContact
    {
        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Cellphone { get; set; }

        public string UId { get; set; }

        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Fullname { get; set; }
    }
}
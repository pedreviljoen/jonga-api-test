﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JongaAPI.Models
{
    public class UserError
    {

        public string message { get; set; }
        public bool noError { get; set; }
        public UserError(string message)
        {
            this.message = message;
            if (message != "ok")
            {
                noError = false;
            }
            else
            {
                noError = true;
            }
        }
    }
}

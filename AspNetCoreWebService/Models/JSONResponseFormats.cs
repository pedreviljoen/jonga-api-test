﻿using JongaAPI.Services.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace JongaAPI.Models
{
    public class JSONResponseFormats<ResponseType, DataType> where ResponseType : new()
    {
        private CommonHelpingMethods _commonHelper = new CommonHelpingMethods();

        public DTOSingleDataResponse<ResponseType, DataType> GetSingleDataResponse(ResponseType typeofResponse, DataType datatype, MetaResponse meta)
        {
            dynamic response = new JObject();
            try
            {
                _commonHelper.MapProp(datatype, typeofResponse);

                //ResponseType response = new ResponseType { };
                // AuthResponseData data = new AuthResponseData { data. };

                return new DTOSingleDataResponse<ResponseType, DataType>(typeofResponse, datatype, meta);
            }
            catch (Exception e)
            {
                response.Error = e.ToString();
                return new DTOSingleDataResponse<ResponseType, DataType>(typeofResponse, datatype, meta); ;
            }
        }

        public DTODataListResponse<ResponseType, DataType> GetDataListResponse(List<ResponseType> typeofResponses, List<DataType> datatypes, MetaResponse meta)
        {
            dynamic response = new JObject();
            try
            {
                for (int i = 0; i < datatypes.Count; i++)
                {
                    typeofResponses.Add(new ResponseType());

                    _commonHelper.MapProp(datatypes[i], typeofResponses[i]);
                }

                return new DTODataListResponse<ResponseType, DataType>(typeofResponses, datatypes, meta);
            }
            catch (Exception e)
            {
                response.Error = e.ToString();
                return new DTODataListResponse<ResponseType, DataType>(typeofResponses, datatypes, meta);
            }
        }

        public DTODataListResponse<ResponseType, DataType> GetCustomDataListResponse(List<ResponseType> typeofResponses, List<DataType> datatypes, MetaResponse meta)
        {
            dynamic response = new JObject();
            try
            {
                for (int i = 0; i < datatypes.Count; i++)
                {
                    _commonHelper.MapProp(datatypes[i], typeofResponses[i]);
                }

                return new DTODataListResponse<ResponseType, DataType>(typeofResponses, datatypes, meta);
            }
            catch (Exception e)
            {
                response.Error = e.ToString();
                return new DTODataListResponse<ResponseType, DataType>(typeofResponses, datatypes, meta);
            }
        }
    }

    public class JSONResponseFormats<ResponseType>
    {
        private CommonHelpingMethods _commonHelper = new CommonHelpingMethods();

        public DTOSingleDataResponse<ResponseType> GetSingleDataResponse(ResponseType typeofResponse, MetaResponse meta)
        {
            dynamic response = new JObject();
            try
            {
                //ResponseType response = new ResponseType { };
                // AuthResponseData data = new AuthResponseData { data. };

                return new DTOSingleDataResponse<ResponseType>(typeofResponse, meta);
            }
            catch (Exception e)
            {
                response.Error = e.ToString();
                return new DTOSingleDataResponse<ResponseType>(typeofResponse, meta); ;
            }
        }

        public DTODataListResponse<ResponseType> GetDataListResponse(List<ResponseType> typeofResponse, MetaResponse meta)
        {
            dynamic response = new JObject();
            try
            {
                //ResponseType response = new ResponseType { };
                // AuthResponseData data = new AuthResponseData { data. };

                return new DTODataListResponse<ResponseType>(typeofResponse, meta);
            }
            catch (Exception e)
            {
                response.Error = e.ToString();
                return new DTODataListResponse<ResponseType>(typeofResponse, meta); ;
            }
        }
    }



}
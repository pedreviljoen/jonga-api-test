﻿namespace JongaAPI.Models
{
    public class DeviceAuthInput
    {
        public string Device_Id { get; set; }
        public string Push_Token { get; set; }
    }
}
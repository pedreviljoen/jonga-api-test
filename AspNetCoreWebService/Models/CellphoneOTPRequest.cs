﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JongaAPI.Models
{
    public class CellphoneOTPRequest
    {
        public string Cellphone { get; set; }
        public string country_code { get; set; }
    }
}

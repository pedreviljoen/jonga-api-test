﻿using System.Collections.Generic;

namespace JongaAPI.Models
{
    public class DTODataListResponse<ResponseType, DataType>
    {
        public DTODataListResponse(IList<ResponseType> responses, IList<DataType> data, MetaResponse meta)
        {
            Data = responses;
            Meta = meta;
        }

        public IList<ResponseType> Data { get; set; }
        public MetaResponse Meta { get; set; }
    }

    public class DTOSingleDataResponse<ResponseType>
    {
        public DTOSingleDataResponse(ResponseType response, MetaResponse meta)
        {
            Data = response;
            Meta = meta;
        }


        public ResponseType Data { get; set; }
        public MetaResponse Meta { get; set; }
    }

    public class DTODataListResponse<ResponseType>
    {
        public DTODataListResponse(List<ResponseType> response, MetaResponse meta)
        {
            Data = response;
            Meta = meta;
        }

        public List<ResponseType> Data { get; set; }
        public MetaResponse Meta { get; set; }
    }

    public class DTOSingleDataResponse<ResponseType, DataType>
    {
        public DTOSingleDataResponse(ResponseType response, DataType data, MetaResponse meta)
        {
            Data = response;
            Meta = meta;
        }

        public ResponseType Data { get; set; }
        public MetaResponse Meta { get; set; }
    }

    public class UserAuthResponseData
    {
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
    }

    public class DeviceAuthResponseData
    {
        public string Id { get; set; }
        public string DeviceToken { get; set; }
        public bool OTP_verified { get; set; } = false;
    }

    public class GetUserResponseData
    {
        public string Cellphone { get; set; }

        public string Fullname { get; set; }

        public int Id { get; set; }

        public string Email { get; set; }

        public bool MobileOTPSent { get; set; }

        public bool OTPVerified { get; set; }
    }

    public class MetaResponse
    {
        public int Code { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class AddUserResponse
    {
        public string Cellphone { get; set; }

        public string Fullname { get; set; }

        public int Id { get; set; }
        public string GUID { get; set; }

        public string Email { get; set; }

        //public bool MobileOTPSent { get; set; }

        public bool OTPVerified { get; set; }

    }

    public class RefreshResposne
    {
        public string AccessToken { get; set; }
    }

    public class GetUserProfileResponse
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string BirthDay { get; set; }
        public string PaymentDetails { get; set; }
        public string NotificationSettings { get; set; }
        public string UserProfileImage { get; set; }
    }

    public class UpdateUserProfileResponse
    {
        public string Fullname { get; set; }
        public string BirthDay { get; set; }
        public string PaymentDetails { get; set; }
        public string NotificationSettings { get; set; }
        public string UserProfileImage { get; set; }
    }

    public class LogoutUserResponse
    {
        public string Cellphone { get; set; } //Logged Out User Cellphone
    }

    public class LogoutDeviceResponse
    {
        public string DeviceId { get; set; }
    }

    public class AddEmergencyContactResponse
    {
        public string Fullname { get; set; }
        public string Cellphone { get; set; }
    }

    public class ViewEmergencyContactResponse
    {
        public int Id { get; set; }
        public string Cellphone { get; set; }
        public string country_code { get; set; }
        public string Name { get; set; }
    }

    public class RemoveEmergencyContactResponse
    {
        public string Name { get; set; }
        public string Cellphone { get; set; }
    }

    public class AddIoTDeviceResponse
    {
        public string Id { get; set; }
        public string Battery { get; set; }
        public string State { get; set; }
    }

    public class ViewIoTDeviceResponse
    {
        public string Id { get; set; }
        public string Battery { get; set; }
        public string State { get; set; }
    }

    public class CellphoneOTPResponse
    {
        public string Sid { get; set; }
        public string Status { get; set; }
    }

    public class UpdateIoTDevice
    {
        public string Armed { get; set; }
        public string Battery { get; set; }
        public string timestamp { get; set; }
    }

    public class MessageDTO
    {
        public string Message { get; set; }
    }

    public class OTPSendReponse
    {
        public string Status { get; set; }
    }

    public class OTPVerifyResponse
    {
        public string Cellphone { get; set; }
        public string Fullname { get; set; }
        public bool OTPVerified { get; set; }
        public string Accesstoken { get; set; }
        public string DeviceToken { get; set; }
    }

    public class ResetOTPVerifyResponse
    {
        public string AccessToken { get; set; }
    }

    public class OTPCode
    {
        public string otp_code { get; set; }
        public string cellphone { get; set; }
        public string country_code { get; set; }
        public string device_id { get; set; }
    }

    public class DeleteIoTDeviceResponse
    {
        public string Id { get; set; }
    }

    public class AlertResponse
    {
        public string Status { get; set; }
    }

    public class ResetPasswrod
    {
        public string Country_Code { get; set; }
        public string Cellphone { get; set; }
    }

    public class UserPassword
    {
        public string Password { get; set; }
    }

    public class UserLogin
    {
        public string Cellphone { get; set; }
        public string Password { get; set; }
        public string Device_Id { get; set; }
        public string country_code { get; set; }
    }

    public class ResetPasswrodVerify
    {
        public string country_code { get; set; }
        public string Cellphone { get; set; }
        public string otp_code { get; set; }
    }

    public class IoTDeviceAdd
    {
        public string Device_id { get; set; }
        public string Name { get; set; }
    }

    public class ListDeviceResponse
    {
        public string Id { get; set; }
        public string DeviceToken { get; set; }
        public string PushToken { get; set; }
    }

    public class AddDeviceResponse
    {
        public string Id { get; set; }
    }

    public class UpdateDevice
    {
        public string PushToken { get; set; }
    }

    public class SigfoxData
    {
        public string Data { get; set; }
        public string Device { get; set; }
        public string time { get; set; }
    }





    public class DummyResponse
    {

    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JongaAPI.Models
{
    [Table("DeviceTokens")]
    public class DeviceToken
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [StringLength(450)]
        public int Id { get; set; }

        public DateTime IssuedUtc { get; set; }

        public DateTime ExpiresUtc { get; set; }

        [Required]
        [StringLength(450)]
        public string Token { get; set; }

        public string DeviceId { get; set; }
        //public Device Device { get; set; }
    }

    public class AccessToken
    {
        public string Token { get; set; }
    }

    public class PushToken
    {
        public string Token { get; set; }

        public string DeviceId { get; set; }
        public Device Device { get; set; }
    }
}
﻿namespace JongaAPI.Models
{
    public class TwilioCallbackMessage
    {
        public string SID { get; set; }
        public string Status { get; set; }
    }
}
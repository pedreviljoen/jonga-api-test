﻿namespace JongaAPI.Models
{
    public class UserEmergencyJunction
    {
        public int Id { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyCellphone { get; set; }
        public int UserId { get; set; }
        public string UserUID { get; set; }
        public int EmergencyId { get; set; }
        public string EmergencyUID { get; set; }
        public string Country_Code { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace JongaAPI.Models
{
    public class Device
    {
        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        [Key]
        public string Id { get; set; }

        public int UserId { get; set; }

        public int DeviceTokenId { get; set; }
        public string DeviceToken { get; set; }

        public string Platform { get; set; }

        public string PushToken { get; set; }

        public string IoTDeviceId { get; set; }

        public bool DeviceVerified { get; set; }
    }
}
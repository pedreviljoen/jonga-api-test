﻿using System.ComponentModel.DataAnnotations;

namespace JongaAPI.Models
{
    public class IoTDevice
    {
        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        [Key]
        public string Id { get; set; }

        public string Device_Id { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; }

        public string Battery { get; set; }

        public bool Armed { get; set; } = false;

        public string TimeStamp { get; set; }

        public string Token { get; set; } = "qefqeaghafa";
    }
}
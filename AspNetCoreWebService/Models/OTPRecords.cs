﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JongaAPI.Models
{
    public class OTPRecords
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int UserId { get; set; }
        public string Status { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiredUtc { get; set; }
    }
}
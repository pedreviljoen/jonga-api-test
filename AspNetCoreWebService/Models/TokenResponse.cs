﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JongaAPI.Models
{
    public class TokenResponse
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime TokenExpiration { get; set; }

        public string Email { get; set; }

        public string Cellphone { get; set; }

        public string Fullname { get; set; }

    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JongaAPI.Models
{
    public class User
    {
        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Cellphone { get; set; }

        [Required(AllowEmptyStrings = false), DisplayFormat(ConvertEmptyStringToNull = false), MinLength(6)]
        public string Password { get; set; }

        [MinLength(2), Column(TypeName = "varchar(MAX)")]
        public string Fullname { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key, DisplayFormat(ConvertEmptyStringToNull = false)]
        public int Id { get; set; }

        public string UId { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string AccessToken { get; set; }

        public string CountryCode { get; set; }

        public string Device_Id { get; set; }

        public string BirthDay { get; set; }

        public string UserProfileImage { get; set; }

        public string Address { get; set; }

        public string PaymentDetails { get; set; }

        public string NotificationSettings { get; set; }

        public bool IsResettingPassword { get; set; }
        public DateTime ResettingWindowUntil { get; set; }

        public string LastOTPCode
        { get; set; }

        public bool OTPUsed { get; set; } = false;

        public bool OTPVerified { get; set; } = false;
        public bool OTPBlocked { get; set; } = false;
        public DateTime OTPBlockedUntilUtc { get; set; }

        public int NumberOfOTPSentInMinutes { get; set; } = 0; //3 OTPs within 10 minutes
        public DateTime LastOTPSentTimeUtc { get; set; }
        public DateTime OTPSessionWindow { get; set; }

        public DateTime OTPVeriWinodow { get; set; }
        public int NumberOfOTPVeriInMinutes { get; set; } = 0;
    }

    public class NewUser
    {
        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Fullname { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Cellphone { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Device_Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Push_Token { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Country_Code { get; set; }

        public override string ToString()
        {
            return "User [" + "Password = " + Password + ", Fullname = " + Fullname + "Email = " + Email + "Cellphone = " + Cellphone + "]";
        }
    }
}
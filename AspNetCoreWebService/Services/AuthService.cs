﻿using JongaAPI.Gateway;
using JongaAPI.Models;
using JongaAPI.Services.Helper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace JongaAPI.Services
{
    public class AuthService : IAuthService
    {
        public readonly AppSettings _appSettings;
        public readonly UserDBContext _userDBContext;
        public readonly CommonHelpingMethods _commonmethods = new CommonHelpingMethods();
        private Random _rdm = new Random();

        public AuthService(IOptions<AppSettings> appSettings, UserDBContext userDBContext)
        {
            _appSettings = appSettings.Value;
            _userDBContext = userDBContext;
        }



        //Tested
        public DTOSingleDataResponse<UserAuthResponseData, User> AuthenticateUser(string cellphone, string password, string DeviceId, string countrycode)
        {
            JSONResponseFormats<UserAuthResponseData, User> _json = new JSONResponseFormats<UserAuthResponseData, User>();

            if (cellphone == null || password == null)
            {
                return _json.GetSingleDataResponse(new UserAuthResponseData { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var user = _userDBContext.Users.SingleOrDefault(x => x.Cellphone == cellphone && x.Password == password && x.CountryCode == countrycode);

            if (user == null) //NULL = WRONG DETAILS
            {
                return _json.GetSingleDataResponse(new UserAuthResponseData { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (DeviceId == null)
            {
                return _json.GetSingleDataResponse(new UserAuthResponseData { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.Find(DeviceId);

            if (device != null && device.UserId != -1 && device.UserId != user.Id) //Some other user is using this device
            {
                return _json.GetSingleDataResponse(new UserAuthResponseData { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(3), CommonHelpingMethods.ResponseCodes.BadRequest));
            }





            if (user.OTPVerified == false)
            {
                return _json.GetSingleDataResponse(new UserAuthResponseData { }, user, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(4), CommonHelpingMethods.ResponseCodes.OK));
            }
            user.Device_Id = DeviceId;
            var token = CreateAccessToken(user);
            user.AccessToken = token;
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new UserAuthResponseData { }, user, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(5), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public int ReplaceDeviceToken(DeviceToken old, DeviceToken newDeviceToken)
        {
            if (!(old == null || newDeviceToken == null))
            {
                DeviceToken Device = _userDBContext.DeviceTokens.FirstOrDefault(x => x.DeviceId == old.DeviceId);

                if (Device != null)
                {
                    _userDBContext.DeviceTokens.Remove(old);
                    _userDBContext.DeviceTokens.Add(newDeviceToken);
                    _userDBContext.SaveChanges();
                    return 200;
                }
            }

            return -1;
        }

        //Tested
        public bool IsDeviceExpired(DeviceToken dBDeviceToken)
        {
            if (dBDeviceToken == null)
                throw new ArgumentNullException();

            if (dBDeviceToken.ExpiresUtc < DateTime.UtcNow)
            {
                return true;
            }
            else
                return false;
        }

        //Tested
        public string CreateAccessToken(User user)
        {
            if (user == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.MobilePhone, user.Cellphone),
                    //new Claim(ClaimTypes.Sid, user.Id.ToString()),
                    //new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddMonths(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var accessToken = new JwtSecurityTokenHandler().WriteToken(token);

            return accessToken;
        }

        //Tested
        public int RemoveDeviceToken(DeviceToken DeviceToken)
        {
            if (_userDBContext.DeviceTokens.Find(DeviceToken.Id) == null)
                return -1;

            if (DeviceToken != null)
            {
                _userDBContext.DeviceTokens.Remove(DeviceToken);
                _userDBContext.SaveChanges();
                return 200;
            }

            return -1;
        }

        //Tested
        public DTOSingleDataResponse<AddUserResponse, User> AddUser(NewUser newuser)
        {
            JSONResponseFormats<AddUserResponse, User> _json = new JSONResponseFormats<AddUserResponse, User>();
            if (newuser == null)
            { return _json.GetSingleDataResponse(new AddUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(6), CommonHelpingMethods.ResponseCodes.BadRequest)); }

            if (CellphoneExists(newuser.Country_Code, newuser.Cellphone))
            {
                return _json.GetSingleDataResponse(new AddUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(7), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsPhoneNumber(newuser.Country_Code, newuser.Cellphone))
            {
                return _json.GetSingleDataResponse(new AddUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsPasswordValid(newuser.Password))
            {
                return _json.GetSingleDataResponse(new AddUserResponse { }, null,
                  _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(9), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = ConvertFromNewUserToUserType(newuser);
            if (user == null)
            { return _json.GetSingleDataResponse(new AddUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(10), CommonHelpingMethods.ResponseCodes.BadRequest)); }

            Device addedDevice = null;

            if (newuser.Device_Id == null)
            { return _json.GetSingleDataResponse(new AddUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest)); }

            Device DeviceBeingUsed = _userDBContext.Devices.Find(newuser.Device_Id);

            if (DeviceBeingUsed == null) //CANT FIND THIS DEVICE IN DB
            {
                addedDevice = InitialiseNewDevice(user, newuser);
                return _json.GetSingleDataResponse(new AddUserResponse { }
                , user, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(11), CommonHelpingMethods.ResponseCodes.OK));
            }


            _userDBContext.Add(user);
            _userDBContext.SaveChanges();


            _userDBContext.Users.Attach(user);

            user.UId = Guid.NewGuid().ToString();

            //Device linked to a user, can find this device in DB, just add user, dont do anything to devices!!
            user.Device_Id = null;
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new AddUserResponse { }, user, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(13), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public Device InitialiseNewDevice(User user, NewUser newUser)
        {
            Device AddedDevice = new Device { Id = newUser.Device_Id, PushToken = newUser.Push_Token };
            PushToken PushToken = new PushToken { Token = newUser.Push_Token };



            _userDBContext.Users.Attach(user); //added

            _userDBContext.Devices.Attach(AddedDevice); //added

            _userDBContext.Users.Add(user);


            AddedDevice.UserId = -1;
            AddedDevice.DeviceTokenId = -1;
            user.Device_Id = null;
            _userDBContext.Devices.Add(AddedDevice);

            PushToken.DeviceId = AddedDevice.Id;


            _userDBContext.SaveChanges();

            return AddedDevice;
        }

        //Tested
        public Device InitialiseNewDevice(User user, string pushToken, string DeviceId)
        {
            Device AddedDevice = new Device { Id = DeviceId, PushToken = pushToken, };
            PushToken PushToken = new PushToken { Token = pushToken };
            DeviceToken DeviceToken = CreateDeviceToken(AddedDevice);
            //string TokenString = CreateAccessToken(user);
            //AccessToken AccessToken = new AccessToken { Token = TokenString };

            _userDBContext.Users.Attach(user); //added
                                               //user.AccessToken = AccessToken.Token;

            _userDBContext.Devices.Attach(AddedDevice); //added
            AddedDevice.DeviceToken = DeviceToken.Token;
            //_userDBContext.SaveChanges();

            AddedDevice.UserId = user.Id;
            _userDBContext.Devices.Add(AddedDevice);

            PushToken.DeviceId = AddedDevice.Id;

            _userDBContext.DeviceTokens.Attach(DeviceToken); //added
            _userDBContext.DeviceTokens.Add(DeviceToken);
            _userDBContext.SaveChanges();

            AddedDevice.DeviceTokenId = DeviceToken.Id;

            DeviceToken.DeviceId = AddedDevice.Id;
            user.Device_Id = AddedDevice.Id;

            _userDBContext.SaveChanges();
            return AddedDevice;
        }

        //Tested
        public DeviceToken CreateDeviceToken(Device Device)
        {
            if (Device == null)
            {
                return null;
            }

            var a = DateTime.Now.ToUniversalTime();
            var newDeviceToken = new DeviceToken
            {
                DeviceId = Device.Id,

                Token = Guid.NewGuid().ToString(),
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.AddMonths(3)
            };
            return newDeviceToken;
        }

        //Tested
        public DeviceToken CreateAndSaveDeviceToken(Device Device)
        {
            if (Device == null)
            {
                return null;
            }
            var DeviceDBToken = _userDBContext.DeviceTokens.SingleOrDefault(x => x.DeviceId == Device.Id);

            if (DeviceDBToken != null)
            {
                return null;
            }

            DeviceToken newDeviceToken = CreateDeviceToken(Device);
            _userDBContext.DeviceTokens.Add(newDeviceToken);
            _userDBContext.SaveChanges();

            return newDeviceToken;
        }

        //Tested
        public User ConvertFromNewUserToUserType(NewUser newuser)
        {
            if (_commonmethods.IsAnyPropertyNull(newuser) || _commonmethods.IsThereAnyEmptyStrings(newuser))
            {
                return null;
            }

            if (!_commonmethods.IsEmailInRightFormat(newuser))
            {
                return null;
            }

            if (!_commonmethods.IsPhoneNumber(newuser.Country_Code, newuser.Cellphone))
            {
                return null;
            }

            return new User { Email = newuser.Email, Fullname = newuser.Fullname, Password = newuser.Password, Cellphone = newuser.Cellphone, CountryCode = newuser.Country_Code, Device_Id = newuser.Device_Id, };
        }

        //Tested
        public bool CellphoneExists(string countrycode, string Cellphone)
        {
            bool Exist = _userDBContext.Users.Any<User>(x => x.Cellphone == Cellphone && x.CountryCode == countrycode);
            return Exist;
        }

        //Tested
        public DTOSingleDataResponse<DeviceAuthResponseData, Device> AuthenticateDevice(string accessToken, string DeviceId, string PushToken)
        {
            JSONResponseFormats<DeviceAuthResponseData, Device> _json = new JSONResponseFormats<DeviceAuthResponseData, Device>();



            if (DeviceId == null || DeviceId == "")
            {
                return _json.GetSingleDataResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var Device = _userDBContext.Devices.Find(DeviceId);

            if (accessToken == null || accessToken == "")
            {
                return _json.GetSingleDataResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            var token = accessToken.Replace("Bearer ", "");
            var User = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (User == null) //The cellphone specified is incorrect
            {
                return _json.GetSingleDataResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (User.OTPVerified == false)
            {
                return _json.GetSingleDataResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (Device == null)// Cant find this device in DB, but CAN find the user operating
            {
                if (PushToken == null || PushToken == "")
                {
                    return _json.GetSingleDataResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(15), CommonHelpingMethods.ResponseCodes.BadRequest));
                }
                Device = InitialiseNewDevice(User, PushToken, DeviceId);
            }

            if (!Device.DeviceVerified)
            {
                SendSMSOTP(User.CountryCode, User.Cellphone, false);
                _userDBContext.SaveChanges();
                return _json.GetSingleDataResponse(new DeviceAuthResponseData { OTP_verified = Device.DeviceVerified }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(16), CommonHelpingMethods.ResponseCodes.OK));
            }

            if (Device.UserId == -1) // If device is not linked to a user
            {
                _userDBContext.Devices.Attach(Device);
                _userDBContext.Users.Attach(User);

                //Device.LinkedToUser = true;
                Device.UserId = User.Id; //Link the user and update the user_id of device
                User.Device_Id = Device.Id;
                _userDBContext.SaveChanges();
            }

            if (User.Id != Device.UserId) //User using the device doesnt match one in DB
            {
                return _json.GetSingleDataResponse(null,
                Device, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(3), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            DeviceToken DBDeviceToken = _userDBContext.DeviceTokens.SingleOrDefault(x => x.DeviceId == DeviceId);

            DeviceToken newDeviceToken = CreateDeviceToken(Device);
            if (DBDeviceToken != null)
                ReplaceDeviceToken(DBDeviceToken, newDeviceToken);
            else
            {
                _userDBContext.DeviceTokens.Attach(newDeviceToken);
                _userDBContext.DeviceTokens.Add(newDeviceToken);
                _userDBContext.SaveChanges();
            }
            _userDBContext.Devices.Attach(Device);
            Device.PushToken = PushToken;
            Device.DeviceToken = newDeviceToken.Token;
            Device.DeviceTokenId = newDeviceToken.Id;

            //_userDBContext.SaveChanges();
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new DeviceAuthResponseData { OTP_verified = Device.DeviceVerified }, Device, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(17), CommonHelpingMethods.ResponseCodes.OK));
        }




        //Tested
        public DTOSingleDataResponse<LogoutUserResponse, User> LogoutUser(string accesstoken)
        {
            //Remove the access token and remove latest DeviceId
            JSONResponseFormats<LogoutUserResponse, User> _json = new JSONResponseFormats<LogoutUserResponse, User>();
            if (accesstoken == null || accesstoken == "")
            {
                return _json.GetSingleDataResponse(new LogoutUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            var user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);
            if (user == null)
            {
                return _json.GetSingleDataResponse(new LogoutUserResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19),
                    CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.Users.Attach(user);
            user.AccessToken = null;
            _userDBContext.SaveChanges();
            return _json.GetSingleDataResponse(new LogoutUserResponse { }, user, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(20),
                    CommonHelpingMethods.ResponseCodes.OK));
        }



        //Tested
        public DTOSingleDataResponse<OTPSendReponse> SendSMSOTP(string countrycode, string cellphone, bool reg)
        {
            JSONResponseFormats<OTPSendReponse> _json = new JSONResponseFormats<OTPSendReponse>();
            if (countrycode == null || countrycode == "" || cellphone == null || cellphone == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.FirstOrDefault(x => x.CountryCode == countrycode && x.Cellphone == cellphone);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (user.OTPVerified && reg)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("User Already Verified", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!ShouldSMSbeSent(user))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(21), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            string code = _rdm.Next(1000, 9999).ToString();
            user.LastOTPCode = code;
            user.OTPUsed = false;



            _userDBContext.SaveChanges();
            TwilioGateway.SendSMS("Jonga OTP: " + code, "+" + user.CountryCode + user.Cellphone);

            return _json.GetSingleDataResponse(new OTPSendReponse { Status = "sent" }, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(22), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<OTPSendReponse> SendSMSOTP(string countryCode, string Cellphone)
        {//Possible to block a user forever!!
            JSONResponseFormats<OTPSendReponse> _json = new JSONResponseFormats<OTPSendReponse>();
            if (countryCode == null || countryCode == "" || Cellphone == null || Cellphone == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(23), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.FirstOrDefault(x => x.CountryCode == countryCode && x.Cellphone == Cellphone);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!ShouldSMSbeSent(user))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(21), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            user.OTPUsed = false;


            string code = _rdm.Next(1000, 9999).ToString();
            user.IsResettingPassword = true;
            user.ResettingWindowUntil = DateTime.UtcNow.AddMinutes(5);
            user.LastOTPCode = code;
            _userDBContext.SaveChanges();
            TwilioGateway.SendSMS("Jonga OTP: " + code, "+" + user.CountryCode + user.Cellphone);

            return _json.GetSingleDataResponse(new OTPSendReponse { Status = "sent" }, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(22), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public bool ShouldSMSbeSent(User user)
        {
            if (user == null)
                return false;

            // 3 Otps Every 10 minutes
            _userDBContext.Users.Attach(user);
            if (user.OTPBlocked)
            {
                if (user.OTPBlockedUntilUtc > DateTime.UtcNow) //Still being blocked
                    return false;
                else //Reset Block Status and number of OTP sent within 10 minutes to 1 (this one being sent!)
                {
                    //Establish a session:
                    user.OTPSessionWindow = DateTime.UtcNow.AddMinutes(10);
                    user.OTPBlocked = false;
                    user.NumberOfOTPSentInMinutes = 1;
                    user.LastOTPSentTimeUtc = DateTime.UtcNow;

                    user.OTPUsed = false;
                    //_userDBContext.SaveChanges();
                    return true;
                }
            }
            else // Currently not beling Blocked!
            {
                //Is OTP session expired? if so, renew
                if (DateTime.UtcNow > user.OTPSessionWindow)
                {
                    user.OTPSessionWindow = DateTime.UtcNow.AddMinutes(10);
                    user.NumberOfOTPSentInMinutes = 0;
                }
                user.LastOTPSentTimeUtc = DateTime.UtcNow;
                if (user.NumberOfOTPSentInMinutes >= 2)
                {
                    user.OTPBlocked = true;
                    user.OTPBlockedUntilUtc = DateTime.UtcNow.AddMinutes(10);
                    user.NumberOfOTPSentInMinutes++;
                }

                user.NumberOfOTPSentInMinutes++;
                // _userDBContext.SaveChanges();
                return true;
            }
        }

        //Tested
        public bool IsUserBlockedForOTPVerfi(User user)
        {
            // 3 verifications in 10 minutes
            if (DateTime.UtcNow.AddMinutes(-10) >= user.OTPVeriWinodow)
            {
                user.OTPVeriWinodow = DateTime.UtcNow.AddMinutes(10);
                user.NumberOfOTPVeriInMinutes = 0;
            }

            if (user.NumberOfOTPVeriInMinutes >= 3)
            {
                return true;
            }

            user.NumberOfOTPVeriInMinutes++;
            _userDBContext.SaveChanges();

            return false;
        }

        //Tested,for registration
        public DTOSingleDataResponse<OTPVerifyResponse> VerifyOTP(string code, string cellphone, string countrycode, string DeviceId)
        {
            JSONResponseFormats<OTPVerifyResponse> _json = new JSONResponseFormats<OTPVerifyResponse>();

            if (code == null || code == "" || cellphone == null || cellphone == "" || countrycode == null || countrycode == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (DeviceId == null || DeviceId == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.FirstOrDefault(x => x.Cellphone == cellphone && x.CountryCode == countrycode);
            _userDBContext.Users.Attach(user);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (IsUserBlockedForOTPVerfi(user))
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(28), CommonHelpingMethods.ResponseCodes.BadRequest));

            if (!(code == user.LastOTPCode) || DateTime.UtcNow > user.OTPSessionWindow || user.OTPUsed == true)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(24), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device DeviceBeingUsed = _userDBContext.Devices.Find(DeviceId);


            if (DeviceBeingUsed == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Confirm OTP with the registered cellphone please!", CommonHelpingMethods.ResponseCodes.BadRequest));
            }


            if (DeviceBeingUsed.UserId == -1) // CAN FIND THIS DEVICE, BUT ITS NOT LINKED TO A USER
            {
                _userDBContext.Devices.Attach(DeviceBeingUsed);
                user.Device_Id = DeviceBeingUsed.Id;
                DeviceBeingUsed.UserId = user.Id;

                var devicetoken = CreateDeviceToken(DeviceBeingUsed);
                _userDBContext.DeviceTokens.Add(devicetoken);
                _userDBContext.SaveChanges();
                DeviceBeingUsed.DeviceToken = devicetoken.Token;
                DeviceBeingUsed.DeviceTokenId = devicetoken.Id;
                DeviceBeingUsed.DeviceVerified = true;
                _userDBContext.SaveChanges();
            }

            DeviceBeingUsed.DeviceVerified = true;
            user.OTPVerified = true;
            user.OTPUsed = true;
            var token = CreateAccessToken(user);
            user.AccessToken = token;

            _userDBContext.SaveChanges();
            return _json.GetSingleDataResponse(new OTPVerifyResponse { Cellphone = user.Cellphone, Fullname = user.Fullname, OTPVerified = user.OTPVerified, Accesstoken = token, DeviceToken = DeviceBeingUsed.DeviceToken }, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(29), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested,password reset
        public DTOSingleDataResponse<ResetOTPVerifyResponse> VerifyOTP(string code, string cellphone, string countrycode)
        {
            JSONResponseFormats<ResetOTPVerifyResponse> _json = new JSONResponseFormats<ResetOTPVerifyResponse>();

            if (code == null || code == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(24), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.FirstOrDefault(x => x.Cellphone == cellphone && x.CountryCode == countrycode);

            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (IsUserBlockedForOTPVerfi(user))
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(28), CommonHelpingMethods.ResponseCodes.BadRequest));

            if (DateTime.UtcNow > user.ResettingWindowUntil || user.OTPUsed == true || !user.IsResettingPassword || !(code == user.LastOTPCode))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(24), CommonHelpingMethods.ResponseCodes.BadRequest));
            }



            user.OTPUsed = true;
            var token = CreateAccessToken(user);
            user.AccessToken = token;
            _userDBContext.SaveChanges();
            return _json.GetSingleDataResponse(new ResetOTPVerifyResponse { AccessToken = token }, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(25), CommonHelpingMethods.ResponseCodes.OK));
        }


        //Tested
        public DTOSingleDataResponse<MessageDTO> ResetMyPassword(string accesstoken, string password)
        {
            JSONResponseFormats<MessageDTO> _json = new JSONResponseFormats<MessageDTO>();
            if (accesstoken == null || accesstoken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");

            User user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!user.IsResettingPassword || DateTime.UtcNow > user.ResettingWindowUntil)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(26), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.Users.Attach(user);

            if (!_commonmethods.IsPasswordValid(password))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(9), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            user.Password = password;
            user.IsResettingPassword = false;
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(27), CommonHelpingMethods.ResponseCodes.OK));
        }

        public void RecordTwilioCallbacks(TwilioCallbackMessage message)
        {
        }
    }
}
﻿using JongaAPI.Gateway;
using JongaAPI.Models;
using JongaAPI.Services.Helper;
using JongaAPI.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using System.Threading.Tasks;

namespace JongaAPI.Services
{
    public class IoTDeviceService : IIoTDeviceService
    {
        public readonly CommonHelpingMethods _commonmethods = new CommonHelpingMethods();
        public readonly UserDBContext _userDBContext;



        public IoTDeviceService(UserDBContext userDBContext)
        {
            _userDBContext = userDBContext;
        }

        //Tested
        public DTOSingleDataResponse<AddIoTDeviceResponse> LinkIoTDevice(string IoTDeviceId, string name, string DeviceToken) //add IoT device to a device!!
        {
            JSONResponseFormats<AddIoTDeviceResponse> _json = new JSONResponseFormats<AddIoTDeviceResponse>();
            if (IoTDeviceId == null || IoTDeviceId == "" || DeviceToken == null || DeviceToken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Device Token or DeviceId invalid", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (name == null || name == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("iot name missing", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            string token = DeviceToken.Replace("Bearer ", "");
            DeviceToken devicetoken = _userDBContext.DeviceTokens.FirstOrDefault(x => x.Token == token);

            if (devicetoken == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Device Token Is Invalid", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsDeviceTokenValid(devicetoken))
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("Device Token is Invalid or Expired", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.FirstOrDefault(x => x.DeviceToken == token);

            if (device == null)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("Can Not Find Device with Specified Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (device.IoTDeviceId == IoTDeviceId)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("Device is linked to iot device already", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (device.IoTDeviceId != null)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("Device Paired to Other IoT Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }


            User user = _userDBContext.Users.Find(_userDBContext.Devices.Find(device.Id).UserId);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("Can Not Find User With Specified Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (user.OTPVerified == false)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("User OTP pending", CommonHelpingMethods.ResponseCodes.BadRequest));
            }



            var IoTDeviceinDB = _userDBContext.IoTDevices.Find(IoTDeviceId);

            if (IoTDeviceinDB != null)
            {
                if (IoTDeviceinDB.UserId == user.Id)
                {
                    return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("This IoT device is already paired to the user!", CommonHelpingMethods.ResponseCodes.BadRequest));
                }

                if (IoTDeviceinDB.UserId != user.Id && IoTDeviceinDB.UserId != -1)
                {
                    return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("This IoT Device is Already Paired to Another User!", CommonHelpingMethods.ResponseCodes.BadRequest));
                }

                if (IoTDeviceinDB.UserId == -1)
                {
                    _userDBContext.Devices.Attach(device);
                    IoTDeviceinDB.UserId = user.Id;
                    IoTDeviceinDB.Id = IoTDeviceId;
                    IoTDeviceinDB.Device_Id = device.Id;
                    IoTDeviceinDB.Name = name;
                    device.IoTDeviceId = IoTDeviceinDB.Id;
                    _userDBContext.SaveChanges();
                    return _json.GetSingleDataResponse(new AddIoTDeviceResponse { Id = IoTDeviceId },
                        _commonmethods.GenerateMetaResponse("IoT Device Already Exists but not linked in DB, and it is now paired to the current user"
                        , CommonHelpingMethods.ResponseCodes.OK));
                }
            }

            IoTDeviceinDB = new IoTDevice { UserId = user.Id, Id = IoTDeviceId, Device_Id = device.Id };
            _userDBContext.IoTDevices.Attach(IoTDeviceinDB);
            _userDBContext.IoTDevices.Add(IoTDeviceinDB);
            device.IoTDeviceId = IoTDeviceinDB.Id;
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new AddIoTDeviceResponse { Id = IoTDeviceId },
                        _commonmethods.GenerateMetaResponse("IoT Device is added to DB and linked to the user"
                        , CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTODataListResponse<ViewIoTDeviceResponse, IoTDevice> ListMyIoTDevices(string DeviceToken)
        {
            JSONResponseFormats<ViewIoTDeviceResponse, IoTDevice> _json = new JSONResponseFormats<ViewIoTDeviceResponse, IoTDevice>();
            List<IoTDevice> MyEmergencyContacts = new List<IoTDevice>();
            if (DeviceToken == null)
            {
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse("Device Token Is Empty", CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            var token = DeviceToken.Replace("Bearer ", "");
            DeviceToken devicetoken = _userDBContext.DeviceTokens.FirstOrDefault(x => x.Token == token);

            if (devicetoken == null)
            {
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse("Device Token Is Invalid", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsDeviceTokenValid(devicetoken))
            {
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse("Device Token Is Expired", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.FirstOrDefault(x => x.DeviceToken == token);

            if (device == null)
            {
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse("Can Not Find Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.Find(_userDBContext.Devices.Find(device.Id).UserId);
            if (user == null)
            {
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse("Can not Find User", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (user.OTPVerified == false)
            {
                return _json.GetDataListResponse(null, null,
                    _commonmethods.GenerateMetaResponse("User OTP pending", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var IoTDevicesinDB = _userDBContext.IoTDevices.Where(x => x.UserId == user.Id).ToList();

            List<ViewIoTDeviceResponse> ReadResponses = new List<ViewIoTDeviceResponse>();

            return _json.GetDataListResponse(ReadResponses, IoTDevicesinDB, _commonmethods.GenerateMetaResponse
                ("IoT Devices Retrieved", CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<UpdateIoTDevice> UpdateMyIoTDevice(string IoTDeviceId, string Armed, string Battery, string DeviceToken, string timestamp)
        {
            JSONResponseFormats<UpdateIoTDevice> _json = new JSONResponseFormats<UpdateIoTDevice>();
            if ((Battery == null || Battery == "") && (Armed == null || Armed == "") && (timestamp == null || timestamp == ""))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Update Fields are Empty", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (DeviceToken == null || DeviceToken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Device Token is Empty or Invalid", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            string token = DeviceToken.Replace("Bearer ", "");

            DeviceToken devicetoken = _userDBContext.DeviceTokens.FirstOrDefault(x => x.Token == token);

            if (devicetoken == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Device Token Can not be found", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsDeviceTokenValid(devicetoken))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Device Token is already expired!", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.Find(devicetoken.DeviceId);

            if (device == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Not Able to Retrive such device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (device.IoTDeviceId != IoTDeviceId)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("No IoT-Cellphone Pair", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.Find(device.UserId);

            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Not able to find corresponding user", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (user.OTPVerified == false)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse("User OTP pending", CommonHelpingMethods.ResponseCodes.BadRequest));
            }


            IoTDevice iotdevice = _userDBContext.IoTDevices.Find(IoTDeviceId);
            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Cant find IOT Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.IoTDevices.Attach(iotdevice);


            if (Armed != null && Armed != "")
            {
                if (Armed == "Armed")
                    iotdevice.Armed = true;
                else if (Armed == "Unarmed")
                    iotdevice.Armed = false;
            }


            if (Battery != null && Battery != "")
            {
                iotdevice.Battery = Battery;
            }

            if (timestamp != null && timestamp != "")
            {
                iotdevice.TimeStamp = timestamp;
            }
            _userDBContext.SaveChanges();
            return _json.GetSingleDataResponse(new UpdateIoTDevice { Battery = iotdevice.Battery, Armed = Armed, timestamp = iotdevice.TimeStamp }, _commonmethods.GenerateMetaResponse("IoT Device Info Updated!", CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<DeleteIoTDeviceResponse> UnlinkMyIoTDevice(string IoTDeviceId, string DeviceToken)
        {
            JSONResponseFormats<DeleteIoTDeviceResponse> _json = new JSONResponseFormats<DeleteIoTDeviceResponse>();

            if (DeviceToken == null || DeviceToken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = DeviceToken.Replace("Bearer ", "");
            DeviceToken devicetoken = _userDBContext.DeviceTokens.FirstOrDefault(x => x.Token == token);

            if (devicetoken == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsDeviceTokenValid(devicetoken))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.Find(devicetoken.DeviceId);

            if (device == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (device.IoTDeviceId != IoTDeviceId)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(3), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.Find(device.UserId);

            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (user.OTPVerified == false)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            IoTDevice iotdevice = _userDBContext.IoTDevices.Find(IoTDeviceId);
            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (user.Id != iotdevice.UserId)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(3), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.Devices.Attach(device);
            device.IoTDeviceId = null;
            _userDBContext.IoTDevices.Remove(iotdevice);
            _userDBContext.SaveChanges();
            return _json.GetSingleDataResponse(new DeleteIoTDeviceResponse { Id = IoTDeviceId }, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(4), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<AlertResponse> AlertUserEmegency(string IoTDeviceId)
        {
            JSONResponseFormats<AlertResponse> _json = new JSONResponseFormats<AlertResponse>();
            if (IoTDeviceId == null || IoTDeviceId == "")
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(5), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            IoTDevice iotdevice = _userDBContext.IoTDevices.Find(IoTDeviceId);

            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            User user = _userDBContext.Users.Find(iotdevice.UserId);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null,
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(6), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var emergencycontactidlist = _userDBContext.UserEmergencyJunctions.Where(x => x.UserId == user.Id).Select(y => y.EmergencyId).ToList();
            var EmergencyContacts = _userDBContext.EmergencyContacts.Where(x => emergencycontactidlist.Contains(x.Id)).ToList();
            AlertUserEmergencies(user, EmergencyContacts);


            return _json.GetSingleDataResponse(new AlertResponse { Status = "sent" },
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(7), CommonHelpingMethods.ResponseCodes.OK));
        }

        private void AlertUserEmergencies(User user, List<EmergencyContact> emergencyContacts)
        {
            string message = user.Fullname + "'s Place is Under Attack! Go help him!!! ";
            foreach (EmergencyContact contact in emergencyContacts)
            {
                TwilioGateway.SendSMS(message, "+" + contact.Country_Code + contact.Cellphone);
            }
        }


        //Not Published for POC!
        public DTOSingleDataResponse<DummyResponse> ArmDisarmIoTDevice(string IoTDeviceId, string DeviceToken, string intent)
        {
            JSONResponseFormats<DummyResponse> _json = new JSONResponseFormats<DummyResponse>();
            if (IoTDeviceId == null || IoTDeviceId == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (DeviceToken == null || DeviceToken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = DeviceToken.Replace("Bearer ", "");
            DeviceToken devicetoken = _userDBContext.DeviceTokens.FirstOrDefault(x => x.Token == token);

            if (devicetoken == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsDeviceTokenValid(devicetoken))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Device Token is already expired!", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.Find(devicetoken.DeviceId);
            if (device == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            IoTDevice iotdevice = _userDBContext.IoTDevices.Find(IoTDeviceId);
            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (device.UserId != iotdevice.UserId)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(9), CommonHelpingMethods.ResponseCodes.BadRequest));

            }

            if (intent != "arm" && intent != "disarm")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(13), CommonHelpingMethods.ResponseCodes.BadRequest));
            }


            if (!iotdevice.Armed && intent == "arm")
            {
                _userDBContext.IoTDevices.Attach(iotdevice);
                iotdevice.Armed = true;
                _userDBContext.SaveChanges();
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(10), CommonHelpingMethods.ResponseCodes.OK));
            }

            if (iotdevice.Armed && intent == "disarm")
            {
                _userDBContext.IoTDevices.Attach(iotdevice);
                iotdevice.Armed = false;
                _userDBContext.SaveChanges();
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(11), CommonHelpingMethods.ResponseCodes.OK));
            }

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(12), CommonHelpingMethods.ResponseCodes.BadRequest));
        }


        //Tested
        public DTOSingleDataResponse<DummyResponse> PushToDeviceAsync(string iotdeviceid)
        {
            JSONResponseFormats<DummyResponse> _json = new JSONResponseFormats<DummyResponse>();
            if (iotdeviceid == null || iotdeviceid == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            IoTDevice iotdevice = _userDBContext.IoTDevices.Find(iotdeviceid);
            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.Find(iotdevice.Device_Id);
            if (device == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.BadRequest));
            }


            var message = new Message()
            {
                //Notification = new Notification { Body = "Your Sensor Detected Motion", Title = "JONGA Alarm" },
                Android = new AndroidConfig
                {
                    Notification = new AndroidNotification
                    {
                        Body = "Your Sensor Detected Motion",
                        Title = "JONGA Alarm",
                        Sound = "default"
                    }

                },
                Token = device.PushToken
            };
            var result = Firebase.SendPushNotiAsync(message);
            Task.WaitAll(result);

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("sent", CommonHelpingMethods.ResponseCodes.OK));
        }

        public Dictionary<string, string> ReadSigfoxData(string data)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();




            string Battery = data.Substring(0, 2);
            string Movement = data.Substring(2, 2);
            string Armed = data.Substring(4, 2);
            dict.Add("Battery", int.Parse(Battery, System.Globalization.NumberStyles.HexNumber).ToString());

            if (Movement == "01")
            { dict.Add("Motion", "Detected"); }

            else if (Movement == "00")
            { dict.Add("Motion", "Undetected"); }

            if (Armed == "01")
            { dict.Add("Arm", "Armed"); }

            else if (Armed == "00")
            { dict.Add("Arm", "Unarmed"); }

            return dict;
        }


        public DTOSingleDataResponse<DummyResponse> UpdateByIoTDevice(string IoTDeviceId, string Battery, string timestamp)
        {
            JSONResponseFormats<DummyResponse> _json = new JSONResponseFormats<DummyResponse>();

            if (IoTDeviceId == null || IoTDeviceId == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Cant find IOT Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            IoTDevice iotdevice = _userDBContext.IoTDevices.Find(IoTDeviceId);
            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Cant find IOT Device", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.IoTDevices.Attach(iotdevice);
            iotdevice.Battery = Battery;
            iotdevice.TimeStamp = timestamp;
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("IoT Device Info Updated!", CommonHelpingMethods.ResponseCodes.OK));

        }

        public DTOSingleDataResponse<DummyResponse> IsSigfoxAuth(string iotid, string sigfoxtoken)
        {
            JSONResponseFormats<DummyResponse> _json = new JSONResponseFormats<DummyResponse>();
            if (iotid == null || iotid == "" || sigfoxtoken == null || sigfoxtoken == "")
            {

                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var iotdevice = _userDBContext.IoTDevices.Find(iotid);
            if (iotdevice == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.IoTDeviceServiceResponses.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            var sigtoken = sigfoxtoken.Replace("Bearer ", "");
            if (iotdevice.Token != sigtoken)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Incorrect Sigfox Token", CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(null, CommonHelpingMethods.ResponseCodes.OK));
        }
    }
}
﻿using JongaAPI.Models;
using JongaAPI.Services.Helper;
using JongaAPI.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace JongaAPI.Services
{
    public class DeviceService : IDeviceService
    {
        public readonly UserDBContext _userDBContext;
        public readonly CommonHelpingMethods _commonmethods = new CommonHelpingMethods();

        public DeviceService(UserDBContext userDBContext)
        {
            _userDBContext = userDBContext;
        }

        //Tested
        public DTOSingleDataResponse<LogoutDeviceResponse, Device> LogOutSingleDevice(string DeviceToken)
        {
            JSONResponseFormats<LogoutDeviceResponse, Device> _json = new JSONResponseFormats<LogoutDeviceResponse, Device>();

            if (DeviceToken == null || DeviceToken == "")
            {
                return _json.GetSingleDataResponse(new LogoutDeviceResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(0),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            DeviceToken = DeviceToken.Replace("Bearer ", "");
            var device = _userDBContext.Devices.FirstOrDefault(x => x.DeviceToken == DeviceToken);
            if (device == null || device.UserId == -1)
            {
                return _json.GetSingleDataResponse(new LogoutDeviceResponse { }, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(1),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.Devices.Remove(device);
            _userDBContext.SaveChanges();

            var devicetoken = _userDBContext.DeviceTokens.FirstOrDefault(x => x.Token == DeviceToken);
            if (devicetoken != null)
                _userDBContext.DeviceTokens.Remove(devicetoken);

            device.DeviceToken = null;
            device.DeviceTokenId = -1;

            User user = _userDBContext.Users.Find(device.UserId);
            if (user != null)
            {
                user.Device_Id = null;
            }

            _userDBContext.SaveChanges();
            return _json.GetSingleDataResponse(new LogoutDeviceResponse { }, device, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(2),
                 CommonHelpingMethods.ResponseCodes.OK));
        }
        //Tested
        public DTOSingleDataResponse<DummyResponse> LogOutAllDevice(string accesstoken)
        {
            JSONResponseFormats<DummyResponse> _json = new JSONResponseFormats<DummyResponse>();
            if (accesstoken == null || accesstoken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            User user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var userdevices = _userDBContext.Devices.Where(x => x.UserId == user.Id);

            if (userdevices.Count() == 0)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(4),
                 CommonHelpingMethods.ResponseCodes.OK));
            }

            var device_id_list = userdevices.Select(x => x.Id).ToList();

            var devicetokens_list = _userDBContext.DeviceTokens.Where(x => device_id_list.Contains(x.DeviceId)).ToList();
            _userDBContext.DeviceTokens.RemoveRange(devicetokens_list);
            _userDBContext.Devices.RemoveRange(userdevices.ToList());
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(4),
                 CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTODataListResponse<ListDeviceResponse, Device> ListMyDevices(string accesstoken)
        {
            JSONResponseFormats<ListDeviceResponse, Device> _json = new JSONResponseFormats<ListDeviceResponse, Device>();
            if (accesstoken == null || accesstoken == "")
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));

            var token = accesstoken.Replace("Bearer ", "");
            User user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);
            if (user == null)
                return _json.GetDataListResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));

            var devices = _userDBContext.Devices.Where(x => x.UserId == user.Id).ToList();
            List<ListDeviceResponse> responses = new List<ListDeviceResponse>();

            return _json.GetDataListResponse(responses, devices, _commonmethods.GenerateMetaResponse("Devices Retrieved", CommonHelpingMethods.ResponseCodes.OK));
        }
        //Tested
        public DTOSingleDataResponse<DummyResponse> UpdateMyDevice(string accesstoken, string PushToken, string DeviceId)
        {
            JSONResponseFormats<DummyResponse> _json = new JSONResponseFormats<DummyResponse>();
            if (accesstoken == null || accesstoken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (DeviceId == null || DeviceId == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(15),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (PushToken == null || PushToken == "")
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(2),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }


            var token = accesstoken.Replace("Bearer ", "");
            User user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            Device device = _userDBContext.Devices.Find(DeviceId);

            if (device == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.DeviceServiceResponses.GetValueOrDefault(1),
                 CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.Devices.Attach(device);
            device.PushToken = PushToken;
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse("Update Succesful!",
                 CommonHelpingMethods.ResponseCodes.OK));


        }
    }
}
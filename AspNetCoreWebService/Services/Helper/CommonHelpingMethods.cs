﻿using JongaAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace JongaAPI.Services.Helper
{
    public class CommonHelpingMethods
    {
        public static readonly Dictionary<int, string> ResponseMessage = new Dictionary<int, string>
        {
            { 0, "Please Fill in Cellphone and/or Password!"},
            { 1, "Incorrect User Credential!" },
            { 2, "Please Provide a Device ID!"},
            { 3, "Device Is Alraedy Used By Another User!"},
            { 4, "User Login is Successful, OTP is Still Pending!"},
            { 5, "User Login is Successful!"},
            { 6, "Registered User is Null!"},
            { 7, "Cellphone Alraedy Exists!" },
            { 8, "Cellphone Format is Invalid!"},
            { 9, "Please Include 1 uppercase, 1 number in password and length must be greater than 6 characters!" },
            {10, "Registration Fields are Incomplete!" },
            {11, "Registration Successful, Current Device is added to DB!"},
            {12, "Device Exists But Not Linked, Update Its User!" },
            {13, "Registration Successful, Current Device Already Exists in DB!" },
            {14, "User OTP Is Still Pending!" },
            {15, "Please Fill in Push Token!" },
            {16, "Device Needs OTP Verification!" },
            {17, "Device Authentication Successful!" },
            {18, "Access Token is Empty!"},
            {19, "Can Not Find User!" },
            {20, "User Logged Out Successfully!" },
            {21, "User Denied of OTP Service!" },
            {22, "OTP is Sent!" },
            {23, "Cellphone is Empty!" },
            {24, "Incorrect, Used or Expired OTP Code!" },
            {25, "Password Can Now be Reset!" },
            {26, "Reset Password OTP Required!" },
            {27, "Password is Now Reset!" },
            {28, "User Blocked For OTP Verification!" },
            {29, "User Verified!" }
        };

        public static readonly Dictionary<int, string> UserServiceResponse = new Dictionary<int, string>
        {
            {0, "No User-Emergency Pair" },
            {1, "User Info Retrived!" },
            {2, "Update Fields are Empty!" },
            {3, "User Info Updated!" },
            {4, "Cellphone of the Emergency Contact Already Exists for this User!" },
            {5, "Emergency Contact Added!" },
            {6, "Emergency Contacts Retrieved!" },
            {7, "Can Not Find Specified Emergency Contact!" },
            {8, "Emergency Contact Updated!" },
            {9, "Emergency Contact Deleted!" }
        };

        public static readonly Dictionary<int, string> DeviceServiceResponses = new Dictionary<int, string>
        {
            {0, "Device Token is Empty!" },
            {1, "Device Can Not be Found, Or Not Linked To a User!" },
            {2, "Device Logged Out Successfully!" },
            {3, "Device-IoT Pair Can not be Found!" },
            {4, "All Devices are LoggedOut!" }
        };

        public static readonly Dictionary<int, string> IoTDeviceServiceResponses = new Dictionary<int, string>
        {
            {0, "Device Token Can Not be Found!" },
            {1, "Device Token is Already Expired!" },
            {2, "Can Not Find IoT Device!" },
            {3, "IoT Device Already Belongs to Another User!" },
            {4, "IoT Device Deleted!" },
            {5, "IoT Device is Empty!" },
            {6, "No IoT-User Pair!" },
            {7, "Emergency Message Sent!" },
            {8, "Please Provide an IoTDeviceId!"},
            {9, "The IoT Device and Cellphone Device Belongs to different User!"},
            {10, "IoT Device is Now Armed!"},
            {11, "IoT Device is Now Disarmed!"},
            {12, "Unknown Error!"},
            {13, "Invalid Arm or Disarm Command!"}
        };

        public List<string> GetPropertiesNameOfClass(object pObject)
        {
            List<string> propertyList = new List<string>();
            if (pObject != null)
            {
                foreach (var prop in pObject.GetType().GetProperties())
                {
                    propertyList.Add(prop.Name);
                }
            }
            return propertyList;
        }

        public bool IsAnyPropertyNull(object obj)
        {
            bool isNotNull = obj.GetType().GetProperties()
                            .All(p => p.GetValue(obj) != null);

            return !isNotNull;
        }

        internal bool IsThereAnyEmptyStrings(Object objRequirement)
        {
            bool isEmpty = objRequirement.GetType().GetProperties()
                .Where(pi => pi.GetValue(objRequirement) is string)
                .Select(pi => (string)pi.GetValue(objRequirement))
                   .Any(value => String.IsNullOrEmpty(value));

            return isEmpty;
        }

        internal bool IsEmailInRightFormat(NewUser newuser)
        {
            string Email = newuser.Email;
            var validator = new EmailAddressAttribute();
            return validator.IsValid(Email);
        }

        public bool IsPhoneNumber(string countryCode, string number)
        {
            number = "+" + countryCode + number;
            Regex validateNumber = new Regex(@"^(\+?27|0)[6-8][0-9]{8}$");
            if (validateNumber.IsMatch(number.Trim()))
            {
                return true;
            }

            return false;
        }

        public void MapProp(object sourceObj, object targetObj)
        {
            Type T1 = sourceObj.GetType();
            Type T2 = targetObj.GetType();

            PropertyInfo[] sourceProprties = T1.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] targetProprties = T2.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var sourceProp in sourceProprties)
            {
                object osourceVal = sourceProp.GetValue(sourceObj, null);

                foreach (var targetProj in targetProprties)
                {
                    if (targetProj.Name == sourceProp.Name)
                    {
                        targetProj.SetValue(targetObj, osourceVal);
                    }
                }

                //int entIndex = Array.IndexOf(targetProprties.va, sourceProp);
                //Debug.WriteLine(sourceProp.PropertyType);
                //Debug.WriteLine(targetProprties);
            }
        }

        public bool IsPasswordValid(string pass)
        {
            if (pass == null)
                return false;

            if (pass.Length < 6)
            { return false; }

            if (!(pass.Any(char.IsUpper) && pass.Any(char.IsDigit)))
            { return false; }

            return true;
        }

        public MetaResponse GenerateMetaResponse(string message, ResponseCodes code)
        {
            bool success;

            if (code == ResponseCodes.BadRequest)
            { success = false; }
            else if (code == ResponseCodes.OK)
            { success = true; }
            else
            { throw new ArgumentNullException(); }

            return new MetaResponse { Message = message, Code = (int)code, Success = success };
        }

        public void UpdateProperties(object sourceObj, object targetObj)
        {
            Type T1 = sourceObj.GetType();
            Type T2 = targetObj.GetType();

            PropertyInfo[] sourceProprties = T1.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] targetProprties = T2.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var sourceProp in sourceProprties)
            {
                object osourceVal = sourceProp.GetValue(sourceObj, null);

                if (osourceVal == null || osourceVal.ToString() == "")
                    continue;

                foreach (var targetProj in targetProprties)
                {
                    if (targetProj.Name == sourceProp.Name)
                    {
                        targetProj.SetValue(targetObj, osourceVal);
                    }
                }

                //int entIndex = Array.IndexOf(targetProprties.va, sourceProp);
                //Debug.WriteLine(sourceProp.PropertyType);
                //Debug.WriteLine(targetProprties);
            }
        }

        public bool IsDeviceTokenValid(DeviceToken DeviceToken)
        {
            if (DateTime.UtcNow < DeviceToken.ExpiresUtc)
                return true;
            return false;
        }

        public enum ResponseCodes
        {
            OK = 200,
            BadRequest = 400,
            Unauthorised = 401
        }
    }
}
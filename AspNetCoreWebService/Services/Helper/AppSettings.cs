﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JongaAPI.Services.Helper
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string UserAccountDB { get; set; }
    }
}

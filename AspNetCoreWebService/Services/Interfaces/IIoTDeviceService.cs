﻿using JongaAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JongaAPI.Services.Interfaces
{
    public interface IIoTDeviceService
    {
        DTOSingleDataResponse<AddIoTDeviceResponse> LinkIoTDevice(string IoTDeviceId, string name, string accesstoken);

        DTODataListResponse<ViewIoTDeviceResponse, IoTDevice> ListMyIoTDevices(string accesstoken);

        DTOSingleDataResponse<UpdateIoTDevice> UpdateMyIoTDevice(string id, string Armed, string Battery, string DeviceToken, string timestamp);

        DTOSingleDataResponse<DeleteIoTDeviceResponse> UnlinkMyIoTDevice(string IoTDeviceId, string DeviceToken);

        DTOSingleDataResponse<AlertResponse> AlertUserEmegency(string IoTDeviceId);
        DTOSingleDataResponse<DummyResponse> ArmDisarmIoTDevice(string IoTDeviceId, string DeviceToken, string intent);

        DTOSingleDataResponse<DummyResponse> PushToDeviceAsync(string deviceid);

        Dictionary<string, string> ReadSigfoxData(string data);

        DTOSingleDataResponse<DummyResponse> UpdateByIoTDevice(string IoTDeviceId, string Battery, string timestamp);
        DTOSingleDataResponse<DummyResponse> IsSigfoxAuth(string iotid, string sigfoxtoken);
    }
}
using JongaAPI.Models;

namespace JongaAPI.Services.Interfaces
{
    public interface IUserService
    {
        DTOSingleDataResponse<GetUserProfileResponse, User> GetUserProfile(string accesstoken);

        DTOSingleDataResponse<UpdateUserProfileResponse, User> UpdateUserProfile(string accesstoken, UpdateUserProfileResponse UpdateFields);

        DTOSingleDataResponse<AddEmergencyContactResponse, EmergencyContact> AddEmergencyContact(string accesstoken, string fullname, string cellphone, string CountryCode);

        DTODataListResponse<ViewEmergencyContactResponse> ListMyEmergencyContacts(string accesstoken);

        DTOSingleDataResponse<UpdateEmergencyContact> UpdateMyEmergencyContacts(string UId, string accesstoken, string updatename, string updatecellphone, string CountryCode);

        DTOSingleDataResponse<DeleteEmergencyContact> DeleteMyEmergencyContact(string UId, string accesstoken);
    }
}
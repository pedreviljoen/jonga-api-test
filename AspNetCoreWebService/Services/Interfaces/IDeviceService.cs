﻿using JongaAPI.Models;

namespace JongaAPI.Services.Interfaces
{
    public interface IDeviceService
    {
        DTOSingleDataResponse<LogoutDeviceResponse, Device> LogOutSingleDevice(string DeviceToken);
        DTOSingleDataResponse<DummyResponse> LogOutAllDevice(string accesstoken);
        DTODataListResponse<ListDeviceResponse, Device> ListMyDevices(string accesstoken);
        DTOSingleDataResponse<DummyResponse> UpdateMyDevice(string accesstoken, string PushToken, string DeviceId);
    }
}
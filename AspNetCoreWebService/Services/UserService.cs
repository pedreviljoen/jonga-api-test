﻿using JongaAPI.Models;
using JongaAPI.Services.Helper;
using JongaAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JongaAPI.Services
{
    public class UserService : IUserService
    {
        public readonly UserDBContext _userDBContext;
        public CommonHelpingMethods _commonmethods = new CommonHelpingMethods();

        public UserService(UserDBContext userDBContext)
        {
            _userDBContext = userDBContext;
        }

        //Tested
        public DTOSingleDataResponse<GetUserProfileResponse, User> GetUserProfile(string accesstoken)
        {
            JSONResponseFormats<GetUserProfileResponse, User> _json = new JSONResponseFormats<GetUserProfileResponse, User>();
            if (accesstoken == null)
            {
                return _json.GetSingleDataResponse(null, null,
                _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            var User = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (User == null)
            {
                return _json.GetSingleDataResponse(null, null,
                   _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!User.OTPVerified)
            {
                return _json.GetSingleDataResponse(null, null,
                   _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            return _json.GetSingleDataResponse(new GetUserProfileResponse { }, User,
                _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(1), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<UpdateUserProfileResponse, User> UpdateUserProfile(string accesstoken, UpdateUserProfileResponse UpdateFields)
        {
            JSONResponseFormats<UpdateUserProfileResponse, User> _json = new JSONResponseFormats<UpdateUserProfileResponse, User>();
            if (UpdateFields == null)
            {
                return _json.GetSingleDataResponse(null, null,
                   _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (UpdateFields.BirthDay == null && UpdateFields.Fullname == null && UpdateFields.NotificationSettings == null && UpdateFields.PaymentDetails == null && UpdateFields.UserProfileImage == null)
            {
                return _json.GetSingleDataResponse(null, null,
                   _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (accesstoken == null)
            {
                return _json.GetSingleDataResponse(null, null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            var User = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (User == null)
            {
                return _json.GetSingleDataResponse(null, null,
                   _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!User.OTPVerified)
            {
                return _json.GetSingleDataResponse(null, null,
                   _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            _commonmethods.UpdateProperties(UpdateFields, User);
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new UpdateUserProfileResponse { }, User,
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(3), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<AddEmergencyContactResponse, EmergencyContact> AddEmergencyContact(string accesstoken, string fullname, string cellphone, string CountryCode)
        {
            JSONResponseFormats<AddEmergencyContactResponse, EmergencyContact> _json = new JSONResponseFormats<AddEmergencyContactResponse, EmergencyContact>();
            if (fullname == null || cellphone == null || CountryCode == null || fullname == "" || cellphone == "" || CountryCode == "")
            {
                return _json.GetSingleDataResponse(new AddEmergencyContactResponse { }, null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (accesstoken == null || accesstoken == "")
            {
                return _json.GetSingleDataResponse(new AddEmergencyContactResponse { }, null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            var user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (user == null)
            {
                return _json.GetSingleDataResponse(new AddEmergencyContactResponse { }, null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!user.OTPVerified)
            {
                return _json.GetSingleDataResponse(new AddEmergencyContactResponse { }, null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!_commonmethods.IsPhoneNumber(CountryCode, cellphone))
            {
                return _json.GetSingleDataResponse(new AddEmergencyContactResponse { }, null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var JunctionEntry = _userDBContext.UserEmergencyJunctions.FirstOrDefault(x => x.EmergencyCellphone == cellphone && x.UserId == user.Id && x.Country_Code == CountryCode);

            if (JunctionEntry != null)
            {
                return _json.GetSingleDataResponse(new AddEmergencyContactResponse { }, null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(4), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var EmergencyinDB = _userDBContext.EmergencyContacts.FirstOrDefault(x => x.Cellphone == cellphone && x.Country_Code == CountryCode);

            if (EmergencyinDB == null)
            {
                EmergencyContact addedEmergency = new EmergencyContact { Cellphone = cellphone, Country_Code = CountryCode, UID = Guid.NewGuid().ToString() };
                _userDBContext.EmergencyContacts.Attach(addedEmergency);
                _userDBContext.EmergencyContacts.Add(addedEmergency);
                _userDBContext.SaveChanges();
                EmergencyinDB = addedEmergency;
            }

            JunctionEntry = new UserEmergencyJunction
            {
                EmergencyCellphone = cellphone,
                EmergencyContactName = fullname,
                EmergencyId = EmergencyinDB.Id,
                Country_Code = CountryCode,
                UserId = user.Id,
                EmergencyUID = EmergencyinDB.UID,
                UserUID = user.UId
            };
            //_userDBContext.UserEmergencyJunctions.Attach(JunctionEntry);
            _userDBContext.UserEmergencyJunctions.Add(JunctionEntry);
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new AddEmergencyContactResponse
            {
                Fullname = fullname,
                Cellphone = cellphone
            }, EmergencyinDB,
                    _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(5), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTODataListResponse<ViewEmergencyContactResponse> ListMyEmergencyContacts(string accesstoken)
        {
            JSONResponseFormats<ViewEmergencyContactResponse> _json = new JSONResponseFormats<ViewEmergencyContactResponse>();
            List<UserEmergencyJunction> MyEmergencyContacts = new List<UserEmergencyJunction>();
            if (accesstoken == null)
            {
                return _json.GetDataListResponse(null, _commonmethods.GenerateMetaResponse("AccessToken Is Empty", CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            var token = accesstoken.Replace("Bearer ", "");
            User user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);
            if (user == null)
            {
                return _json.GetDataListResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!user.OTPVerified)
            {
                return _json.GetDataListResponse(null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var EmergencyJunctions = _userDBContext.UserEmergencyJunctions.Where(x => x.UserId == user.Id).ToList();






            List<ViewEmergencyContactResponse> ReadResponses = new List<ViewEmergencyContactResponse>();

            for (int i = 0; i < EmergencyJunctions.Count; i++)
            {

                ReadResponses.Add(new ViewEmergencyContactResponse { Cellphone = EmergencyJunctions[i].EmergencyCellphone, Id = EmergencyJunctions[i].Id, Name = EmergencyJunctions[i].EmergencyContactName, country_code = EmergencyJunctions[i].Country_Code });
            }


            return _json.GetDataListResponse(ReadResponses, _commonmethods.GenerateMetaResponse
                (CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(6), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<UpdateEmergencyContact> UpdateMyEmergencyContacts(string emergencyUid, string accesstoken, string updatename, string updatecellphone, string countryCode)
        {
            JSONResponseFormats<UpdateEmergencyContact> _json = new JSONResponseFormats<UpdateEmergencyContact>();
            if (accesstoken == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (updatecellphone == null && updatename == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(2), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            User user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);
            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!user.OTPVerified)
            {
                return _json.GetSingleDataResponse(null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            var emergencyinDB = _userDBContext.EmergencyContacts.FirstOrDefault(x => x.UID == emergencyUid);

            if (emergencyinDB == null)
            {
                return _json.GetSingleDataResponse(null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(7), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var emergencyUserJunctionDB = _userDBContext.UserEmergencyJunctions.FirstOrDefault(x => x.UserId == user.Id && x.EmergencyId == emergencyinDB.Id
            );

            if (emergencyUserJunctionDB == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(0), CommonHelpingMethods.ResponseCodes.BadRequest));
            }
            if (emergencyinDB == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(7), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            bool phonevalid = _commonmethods.IsPhoneNumber(countryCode, updatecellphone);

            if (!phonevalid && (updatecellphone != null || countryCode != null))
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.EmergencyContacts.Attach(emergencyinDB);
            _userDBContext.UserEmergencyJunctions.Attach(emergencyUserJunctionDB);

            if (updatename != null)
            {
                emergencyUserJunctionDB.EmergencyContactName = updatename;
            }

            if (updatecellphone != null && countryCode != null)
            {
                emergencyinDB.Cellphone = updatecellphone;
                emergencyUserJunctionDB.EmergencyCellphone = updatecellphone;
                emergencyinDB.Country_Code = countryCode;
                emergencyUserJunctionDB.Country_Code = countryCode;
            }

            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new UpdateEmergencyContact { UId = emergencyUid, Cellphone = updatecellphone, Name = updatename, Country_Code = countryCode }

            , _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(8), CommonHelpingMethods.ResponseCodes.OK));
        }

        //Tested
        public DTOSingleDataResponse<DeleteEmergencyContact> DeleteMyEmergencyContact(string EmergencyId, string accesstoken)
        {
            JSONResponseFormats<DeleteEmergencyContact> _json = new JSONResponseFormats<DeleteEmergencyContact>();

            if (accesstoken == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(18), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var token = accesstoken.Replace("Bearer ", "");
            var user = _userDBContext.Users.FirstOrDefault(x => x.AccessToken == token);

            if (user == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(19), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            if (!user.OTPVerified)
            {
                return _json.GetSingleDataResponse(null,
                      _commonmethods.GenerateMetaResponse(CommonHelpingMethods.ResponseMessage.GetValueOrDefault(14), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            var junctioninDB = _userDBContext.UserEmergencyJunctions.FirstOrDefault
                (x => x.EmergencyUID == EmergencyId && x.UserId == user.Id);

            if (junctioninDB == null)
            {
                return _json.GetSingleDataResponse(null, _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(7), CommonHelpingMethods.ResponseCodes.BadRequest));
            }

            _userDBContext.UserEmergencyJunctions.Remove(junctioninDB);
            _userDBContext.SaveChanges();

            return _json.GetSingleDataResponse(new DeleteEmergencyContact { UId = EmergencyId, Cellphone = junctioninDB.EmergencyCellphone, Fullname = junctioninDB.EmergencyContactName },
                _commonmethods.GenerateMetaResponse(CommonHelpingMethods.UserServiceResponse.GetValueOrDefault(9), CommonHelpingMethods.ResponseCodes.OK));
        }
    }
}